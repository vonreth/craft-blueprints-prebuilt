# -*- coding: utf-8 -*-
import info
from Package.AutoToolsPackageBase import *


class subinfo(info.infoclass):
    def setDependencies(self):
        self.buildDependencies["dev-utils/msys"] = None
        self.buildDependencies["dev-utils/pkg-config"] = None
        self.runtimeDependencies["virtual/base"] = None
        self.runtimeDependencies["libs/openssl"] = None
        self.runtimeDependencies["libs/zlib"] = None
        #self.runtimeDependencies["libs/gpgme/gpgme"] = None
        self.runtimeDependencies["libs/pcre2"] = None
        self.runtimeDependencies["libs/gettext"] = None

    def setTargets(self):
        self.description = "GNU Wget is a free software package for retrieving files using HTTP, HTTPS, FTP and FTPS the most widely-used Internet protocols. " \
                           "It is a non-interactive commandline tool, so it may easily be called from scripts, cron jobs, terminals without X-Windows support, etc."
        self.webpage = "https://www.gnu.org/software/wget/"
        for ver in ["1.21.2", "1.24.5"]:
            self.targets[ver] = f"https://ftp.gnu.org/gnu/wget/wget-{ver}.tar.gz"
            self.targetInstSrc[ver] = f"wget-{ver}"
        self.targetDigests["1.21.2"] = (['e6d4c76be82c676dd7e8c61a29b2ac8510ae108a810b5d1d18fc9a1d2c9a2497'], CraftHash.HashAlgorithm.SHA256)
        self.patchToApply["1.21.2"] = [("wget-1.21.2-20220203.diff", 1)]
        self.patchToApply["1.24.5"] = [("wget-1.21.2-20220203.diff", 1)]

        self.targetDigests["1.24.5"] = (['fa2dc35bab5184ecbc46a9ef83def2aaaa3f4c9f3c97d4bd19dcb07d4da637de'], CraftHash.HashAlgorithm.SHA256)

        self.defaultTarget = "1.24.5"


class Package(AutoToolsPackageBase):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.subinfo.options.configure.autoreconf = False
        self.subinfo.options.configure.args += ["--enable-shared", "--disable-static",
                                                 "--with-ssl=openssl"
                                                 ]

    def createPackage(self):
        self.addExecutableFilter(r"bin/(?!(wget)).*")
        self.blacklist_file.append(os.path.join(self.packageDir(), 'blacklist.txt'))
        return TypePackager.createPackage(self)
